#!/bin/bash
set_steam() {
	rm stable/STEAM_API_KEY
	rm dev/STEAM_API_KEY
	ln -s /opt/nautdrafter/STEAM_API_KEY stable/STEAM_API_KEY
	ln -s /opt/nautdrafter/STEAM_API_KEY dev/STEAM_API_KEY
}

# runs the server
run() {
    {
        echo
        echo "--------------------------------------------"
        echo
        echo
        nohup NautDrafter_Lobby/bin/NautDrafter_Lobby $1 $2
    } >>lobby.log 2>&1 &
}

run_with_save() {
    {
        echo
        echo "--------------------------------------------"
        echo
        echo
        nohup NautDrafter_Lobby/bin/NautDrafter_Lobby $1 $2 /opt/nautdrafter/summaries
    } >>lobby.log 2>&1 &
}

print_usage(){
    echo "Usage: server.bash both|stable|dev"
}

kill_java(){
    echo "Killing any running servers"
    echo "(hope you're not running any other java programs :P)"
    killall java
}

setup_servers(){
    kill_java
    echo "Settings Steam API Key"
    set_steam
}

start_stable_server(){
    echo "Starting stable server"
    pushd .
    cd stable
    #run_with_save server.nautdrafter.monky-games.com 50000
    run server.nautdrafter.monky-games.com 50000
    popd
}

start_dev_server(){
    echo "Starting dev server"
    pushd .
    cd dev
    #run server.nautdrafter.monky-games.com 50001
    run_with_save server.nautdrafter.monky-games.com 50001
    popd
}


if [ "$#" -ne 1 ]; then
    print_usage
    exit 1;
fi


if [ $1 == 'both' ]; then

    setup_servers
    start_stable_server
    start_dev_server

elif [ $1 == 'stable' ]; then
    setup_servers
    start_stable_server

elif [ $1 == 'dev' ]; then
    setup_servers
    start_dev_server

else
    print_usage
    exit 1;

fi

echo "Servers started"
