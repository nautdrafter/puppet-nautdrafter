# nautdrafter

Nautdrafter puppet module deploys the [nautdrafter](http://nautdrafter.bitbucket.org/) lobby server and dedicated drafting servers.

## Class: Nautdrafter

### Parameters

#### `servername`
The name of the server.

#### `steam_api_key
The api key used to connect to steam.

#### `lobby_port`
The port the lobby server runs.
Default: 5000

#### `dedicated__server_port_start`
The first port to start the a dedicated drafting server.
Default: 5001

#### `dedicated_server_size`
The number of dedicated servers to use.
Default: 100