# == Class: nautdrafter::service
#
# Manages the nautdrafter service.
#
# === Parameters
#
# === Variables
#
#
class nautdrafter::service{
  anchor{'nautdrafter::service::begin':}
  exec{'start server':
    command     => "${nautdrafter::home}/servers.bash stable",
    cwd         => $nautdrafter::home,
    refreshonly => true,
    require     =>  Anchor['nautdrafter::service::begin']
  }
  anchor{'nautdrafter::service::end':
    require => Exec['start server'],
  }
}
