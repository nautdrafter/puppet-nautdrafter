# == Class: nautdrafter::config
#
#
# === Parameters
#
# === Variables
#
# [*dirs*]
#   An array of directory paths to create
#
#
class nautdrafter::config{
  anchor{'nautdrafter::config::begin':}

  $dirs = [$nautdrafter::home, $nautdrafter::versions, $nautdrafter::downloads]

  $server_version = regsubst($nautdrafter::server_software_url,
    '^.*?([0-9]+\.[0-9]+\.[0-9]+).*?$','\1','G')
  $version_path = "${nautdrafter::versions}/${server_version}"

  notice("server_version = ${server_version}")

  # sets the group ownership on all files to naut
  File{
    group => 'naut'
  }

  group {'naut':
    ensure => present,
    require => Anchor['nautdrafter::config::begin'],
  }

  file {$dirs:
    ensure  => directory,
    require => Group['naut'],
  }

  #copy the server starting file
  file {"${nautdrafter::home}/servers.bash":
    ensure  => file,
    content => template('nautdrafter/servers.bash.erb'),
    require => File[$dirs],
  }

  # copy the steam api key
  file {"${nautdrafter::home}/STEAM_API_KEY":
    ensure  => file,
    content => $nautdrafter::steam_api_key,
    require => File ["${nautdrafter::home}/servers.bash"],
  }

  file {$version_path:
    ensure  => directory,
    require => File ["${nautdrafter::home}/STEAM_API_KEY"],
  }

  file {$nautdrafter::stable:
    ensure  => link,
    target  => $version_path,
    require => File[$version_path],
  }

  # download the url
  wget::fetch{'download server':
    source      => $nautdrafter::server_software_url,
    destination => "${nautdrafter::downloads}/server.tar",
    timeout     => 0,
    verbose     => false,
    require     => File[$nautdrafter::stable],
  }

  exec{'unzip server':
    command     => "/bin/tar -C ${version_path} -xf ${nautdrafter::downloads}/server.tar" ,
    refreshonly => true,
    subscribe   => Wget::Fetch['download server'],
  }

  anchor{'nautdrafter::config::end':
    require => Exec['unzip server']
  }
}
