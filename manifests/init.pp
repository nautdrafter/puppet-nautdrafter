# == Class: nautdrafter
#
# Deploys the nautdrafter server.
#
# === Parameters
#
# [*servername*]
#   The name of the server.
#
# [*server_software_url*]
#   The url of the nautdrafter server to install.
#
# [*steam_api_key*]
#   The api key for steam.
#
# [*lobby_port*]
#   The port the lobby server runs.
#   Default: '5000'
#
# [*dedicated__server_port_start*]
#   The first port to start the a dedicated drafting server.
#   Default: '5001'
#
# [*dedicated_server_size*]
#   The number of dedicated servers to use.
#   Default: '100'
#
# === Variables
#
# [*home*]
#   The directory that will contain the nautdrafter software
#
# [*versions*]
#   The directory that contains the versions.
#
# === Examples
#
#  class { 'nautdrafter':
#  }
#
#
class nautdrafter (
  $servername,
  $steam_api_key,
  $server_software_url,
  $lobby_port                  = '5000',
  $dedicated_server_port_start = '5001',
  $dedicated_server_size       = '100',
){
  anchor{'nautdrafter::begin':}

  # == variables == #
  $home      = '/opt/nautdrafter'
  $downloads = "${home}/downloads"
  $versions  = "${home}/versions"
  $stable    = "${home}/stable"

  # install requirements
  class{'nautdrafter::install':
    require => Anchor['nautdrafter::begin']
  }
  class{'nautdrafter::config':
    require => Class['nautdrafter::install']
  }
  class{'nautdrafter::service':
    subscribe => Class['nautdrafter::config']
  }
  anchor{'nautdrafter::end':
    require => Class['nautdrafter::service'],
  }
}
