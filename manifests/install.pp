# == Class: nautdrafter::install
#
#
# === Parameters
#
# === Variables
#
#
class nautdrafter::install{
  anchor{'nautdrafter::install::begin':}
  class{'java8':
    require => Anchor['nautdrafter::install::begin'],
  }
  anchor{'nautdrafter::install::end':
    require => Class['java8'],
  }
}
